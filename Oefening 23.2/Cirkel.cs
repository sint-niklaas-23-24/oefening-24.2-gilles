﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_23._2
{
    internal class Cirkel
    {
        private double _straal;

        public Cirkel() { }
        public Cirkel(double straal)
        {
            Straal = straal;
        }

        public double Straal
        {
            get { return _straal; }
            set { _straal = value; }
        }

        public double BerekenOmtrek()
        {
            return 2 * Math.PI * Straal;
        }
        public double BerekenOppervlakte()
        {
           return Math.Pow(Straal, 2) * Math.PI;
        }
        public string FormattedOmtrek()
        {
            return BerekenOmtrek().ToString("0.00");
        }
        public string FormattedOppervlakte()
        {
            return BerekenOppervlakte().ToString("0.00");
        }
        public override string ToString()
        {
            return "Omtrek: " + FormattedOmtrek().PadLeft(20) + Environment.NewLine + Environment.NewLine + "Oppervlakte: " + FormattedOppervlakte().PadLeft(15);
        }
    }
}
