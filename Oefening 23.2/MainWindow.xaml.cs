﻿using System;
using System.Media;
using System.Windows;

namespace Oefening_23._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Cirkel cirkel;
        private void btnBereken_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cirkel = new Cirkel(Convert.ToDouble(txtInput.Text));
                lblResultaat.Content = cirkel.ToString();
            }
            catch (FormatException)
            {
                MessageBox.Show("U heeft geen numerieke waarde ingegeven.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
